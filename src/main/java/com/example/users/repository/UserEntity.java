package com.example.users.repository;

import java.time.LocalDate;

import org.hibernate.annotations.ColumnDefault;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "users")
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name="email", nullable = false, unique = true)
	private String email;
	@Column(name="name", length = 50 )
	private String name;
	@Column(name="user_id", nullable = false, unique = true)
	private String userId;
//	@Column(name="create_at", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
//	@ColumnDefault(value = "CURRENT_TIMESTAMP")
	@Column(name="create_at")
	private LocalDate createAt;
	@Column(name="encrypted_password", nullable = false)
	private String encryptedPassword;
	
	@Builder
	public UserEntity(String email, String name, String userId, LocalDate createAt,
			String encryptedPassword) {
		super();
		this.email = email;
		this.name = name;
		this.userId = userId;
		this.createAt = createAt;
		this.encryptedPassword = encryptedPassword;
	}

	@Builder
	public UserEntity(String email, String name, String userId, String encryptedPassword) {
		super();
		this.email = email;
		this.name = name;
		this.userId = userId;
		this.encryptedPassword = encryptedPassword;
	}
	
}