package com.example.users.service;

import java.util.List;

import com.example.users.exception.UnAuthenticationException;

public interface UserService {
	public UserDto createUser(UserDto userDto);
	public List<UserDto> getUsers();
	public UserDto getUser(String userId);
	public UserDto updateUser(UserDto userDto);
	public String deleteUser(String userId);
	
	public UserDto loginCheck(UserDto user) throws UnAuthenticationException;

}
