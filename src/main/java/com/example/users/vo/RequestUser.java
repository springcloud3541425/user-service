package com.example.users.vo;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * pom.xml  jakarta.valication-api dependency 추가
 * 		<!-- https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api -->
		<dependency>
		    <groupId>jakarta.validation</groupId>
		    <artifactId>jakarta.validation-api</artifactId>
		    <version>3.0.2</version>
		</dependency>
	http request data validataion check	Context Object
 */
@Getter
@Setter
@NoArgsConstructor
public class RequestUser {
	@NotNull(message = "Email cannot null")
	@Email
	private String email;
	
	private String name;
	
	@NotNull(message = "Password cannot null")
	@Size(min = 8, message="Password must be equals or greater than 8")
	private String password;
	
	@Builder
	public RequestUser(String email, String name, String password) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
	}
	
	
}
