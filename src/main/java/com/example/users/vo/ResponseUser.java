package com.example.users.vo;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseUser {
	private String email;
	private String userId;
	private String name;
	
	@Builder
	public ResponseUser(String email, String userId, String name) {
		this.email = email;
		this.userId = userId;
		this.name = name;
	}
	
	
}
